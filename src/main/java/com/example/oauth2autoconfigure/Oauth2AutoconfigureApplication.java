package com.example.oauth2autoconfigure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2AutoconfigureApplication {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2AutoconfigureApplication.class, args);
    }

}
