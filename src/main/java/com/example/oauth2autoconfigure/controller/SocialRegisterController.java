package com.example.oauth2autoconfigure.controller;

import com.example.oauth2autoconfigure.domain.AuthResponse;
import com.example.oauth2autoconfigure.domain.FacebookInfo;
import com.example.oauth2autoconfigure.domain.GoogleToken;
import com.example.oauth2autoconfigure.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

@CrossOrigin
@RestController
public class SocialRegisterController {

    private final RegisterService service;

    @Autowired
    public SocialRegisterController(RegisterService service) {
        this.service = service;
    }

    /**
     * send a google login token after user logged in
     */
    @PostMapping("/api/user/google/register")
    public void registerWithGoogle(@RequestBody GoogleToken token) {
        try {
            service.register(token);
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/api/user/facebook/register")
    public void registerWithFacebook(@RequestBody FacebookInfo info) throws UnsupportedEncodingException {
        System.out.println(info.toString());
        String signedRequest = info.getAuthResponse().getSignedRequest();
    }
}
