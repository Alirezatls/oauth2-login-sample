package com.example.oauth2autoconfigure.controller;

import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SocialUserInfoController {

    /**
     * get user Information after login
     */
    @GetMapping("/api/social/info")
    public OAuth2User getGoogleUserInfo(@AuthenticationPrincipal OAuth2User oAuth2User) {
        return oAuth2User;
    }
}
