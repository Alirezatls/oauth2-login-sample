package com.example.oauth2autoconfigure.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @RequestMapping(path = "/google",method = RequestMethod.GET)
    public String googleLoginPage() {
        return "google";
    }


    @RequestMapping(path = "/facebook",method = RequestMethod.GET)
    public String facebookLoginPage() {
        return "facebook";
    }
}
