package com.example.oauth2autoconfigure.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacebookInfo {
    /**
     *  status: 'connected',
     *     authResponse: {
     *         accessToken: '...',
     *         expiresIn:'...',
     *         signedRequest:'...',
     *         userID:'...'
     *     }
     */
    private String status;
    private AuthResponse authResponse;
}
