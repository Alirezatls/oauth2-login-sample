package com.example.oauth2autoconfigure.service;

import com.example.oauth2autoconfigure.domain.GoogleToken;

import java.io.IOException;
import java.security.GeneralSecurityException;

public interface RegisterService {

    void register(GoogleToken token) throws GeneralSecurityException, IOException;
}
