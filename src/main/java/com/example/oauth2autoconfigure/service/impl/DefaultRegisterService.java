package com.example.oauth2autoconfigure.service.impl;

import com.example.oauth2autoconfigure.domain.GoogleToken;
import com.example.oauth2autoconfigure.service.RegisterService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
public class DefaultRegisterService implements RegisterService {
    private static final JacksonFactory jacksonFactory = new JacksonFactory();

    /**
     * verify google token
     */
    @Override
    public void register(GoogleToken token) throws GeneralSecurityException, IOException {
        String clientId = "323184338133-urlvjntp4442e2gc8pej1e66olvu9j8b.apps.googleusercontent.com";
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), jacksonFactory)
                .setAudience(Collections.singletonList(clientId))
                .build();

        GoogleIdToken idToken = verifier.verify(token.getToken());
        if (idToken != null) {
            GoogleIdToken.Payload payload = idToken.getPayload();
            String userId = payload.getSubject();
            System.out.println("User ID: " + userId);
            String email = payload.getEmail();
            boolean emailVerified = payload.getEmailVerified();
            String name = (String) payload.get("name");
            String pictureUrl = (String) payload.get("picture");
            String locale = (String) payload.get("locale");
            String familyName = (String) payload.get("family_name");
            String givenName = (String) payload.get("given_name");

            System.out.println(email);
            System.out.println(name);
            System.out.println(pictureUrl);
            System.out.println(locale);
            System.out.println(familyName);
            System.out.println(emailVerified);
            System.out.println(givenName);
        } else {
            System.out.println("Invalid ID token.");
        }
    }
}
